diary Homework1.out;
%%%%%%%%%%%%HOMEWORK1%%%%%%%%%%%%

%Exercise 1%
X = [1 1.5 3 4 5 7 9 10]
Y1 = -2+0.5*X
Y2=-2+0.5*X.^2

% you should have had the same axis for both functions. Otherwise it looks
% wired.
yyaxis left
plot(X,Y1);
yyaxis right
plot(X,Y2);


%Exercise 2%

X = linspace(-10,20,200);
X = X';
sum(X)


%Exercise 3%

A = [2 4 6 ; 1 7 5 ; 3 12 4]
B =  [-2 3 10]'
C = A'*B
% inv is slow. I prefer using \ operator whereever possible
D = inv(A'*A)*B
E = sum(A*B)
F = A([1,3] , 1:2)
x = A\B

%Exercise 4%
% check out kron() function
B = blkdiag(A, A, A, A, A)



%Exercise 5%
A = normrnd(10,5,5,3)
% A = A>10 would also work here
for j = 1:3
    for i = 1:5
        if A(i,j) <10
            A(i,j) = 0;
        else
            A(i,j) = 1;        
        end
    end
end
A


%Exercise 6%

%Code for importing the data from csv
% please dont use the absolute path since I have a different path on my
% computer
filename = 'datahw1.csv';
delimiter = ',';
formatSpec = '%f%f%f%f%f%f%[^\n\r]';
fileID = fopen(filename,'r');
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'TextType', 'string', 'EmptyValue', NaN,  'ReturnOnError', false);
fclose(fileID);
datahw1 = table(dataArray{1:end-1}, 'VariableNames', {'ID','Year','Export','RD','prod','cap'});
clearvars filename delimiter formatSpec fileID dataArray ans;
%Code for importing the data from csv
fit = fitlm(datahw1,'prod~Export+RD+cap')

diary off;