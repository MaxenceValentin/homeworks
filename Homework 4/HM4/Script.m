clear all;
% load('C:\Users\mav24\Desktop\Working documents\Econ 512\Homework 4\HM4\hw4data.mat')

addpath('C:\Users\mav24\Desktop\Working documents\Econ 512\Homework 3\CEtools');

load('hw4data.mat')

%Remove data from structure
T = data.T;
X = data.X;
Y = data.Y;
Z = data.Z;
N = data.N;
%rng(123);

%%
%%%%%%%%%%%%%%%%%QUESTION 1%%%%%%%%%%%%%%%%%%
mubeta = 0.1;
sigmabeta = 1;
parameter = [mubeta sigmabeta];

%Define nodes and weights
min = (parameter(1)-3*parameter(2)); % I am not sure what is the best way to chose boundaries for the integral
max = (parameter(1)+3*parameter(2));
[node, weight] = qnwlege(20,min,max); 

%Compute Likelihood function using predefined parameters
[L] = likelihood1(X,Y,parameter,node,weight);
% There are severe mistakes in how you calculate inetgral. it invalidates
% all you do here and after. please consult the answer key and lecture
% materials.
disp('The Likelihood function using Gaussian Quadrature integration equals')
L

%%
%%%%%%%%%%%%%%%%%QUESTION 2%%%%%%%%%%%%%%%%%%
mubeta = 0.1;
sigmabeta = 1;
parameter = [mubeta sigmabeta];

%Define nodes and weights
min = (parameter(1)-3*parameter(2)); % I am not sure what is the best way to chose boundaries for the integral
max = (parameter(1)+3*parameter(2));
[node, weight] = qnwequi(100,min,max);

%Compute Likelihood function using predefined parameters
[L] = likelihood2(X,Y,parameter,node,weight);
% same mistakes with coding integral
disp('The Likelihood function using MonteCarlo integration equals')
L
%%
%%%%%%%%%%%%%%%%%QUESTION 3%%%%%%%%%%%%%%%%%%
mubeta0 = 2; %starting value
sigmabeta0 = 1;
initial = [mubeta0 sigmabeta0];

%Define nodes and weights for Gaussian Quadrature
min = (initial(1)-3*initial(2)); % I am not sure what is the best way to chose boundaries for the integral
max = (initial(1)+3*initial(2));
[node, weight] = qnwlege(20,min,max); 

%Minimize the negative of the likelihood function with respect to
%beta(mu,sigma)
options = optimoptions('fminunc','Display','iter','GradObj','off','Algorithm','quasi-newton','HessUpdate','bfgs');
[parameter] = fminunc(@(parameter) (-1)*likelihood1(X,Y,parameter,node,weight),initial,options);

disp('The Likelihood function is maximized at parameters (using GQ integration)')
parameter

%Define nodes and weights for Monte-Carlo Procedure
min = (initial(1)-3*initial(2)); % I am not sure what is the best way to chose boundaries for the integral
max = (initial(1)+3*initial(2));
[node, weight] = qnwequi(100,min,max);

%Minimize the negative of the likelihood function with respect to
%beta(mu,sigma)
[parameter] = fminunc(@(parameter) (-1)*likelihood2(X,Y,parameter,node,weight),initial,options);
disp('The Likelihood function is maximized at parameters (using Monte-Carlo integration)')
parameter

% mistakes in integral don't let you get the correct estimates. 
%%
%%%%%%%%%%%%%%%%%QUESTION 4%%%%%%%%%%%%%%%%%%
mubeta =0.1;
muu = 0.1;
sigmabeta = 1;
sigmau =1 ;
sigmabetau = 0.5;
gamma = 0.2;
parameters = [mubeta muu sigmabeta sigmau sigmabetau gamma];

%Define nodes and weights for both beta and u
min1 = (parameters(1)-3*parameters(3)); 
max1 = (parameters(1)+(3)*parameters(3));
[node1, weight1] = qnwequi(100,min1,max1);

min2 = (parameters(2)-3*parameters(4)); 
max2 = (parameters(2)+(3)*parameters(4));
[node2, weight2] = qnwequi(100,min2,max2);

%Compute likelihood function using predefined parameters
[L] = likelihood3(X,Y,Z,parameters,node1,node2,weight1,weight2);
disp('The Likelihood function using MonteCarlo integration with predefined value as above is')
L

%%
%Maximizing the likelihood function wrt all parameters in model
mubeta_0 = 0.1;
muu_0 = 0.1;
sigmabeta_0 = 1;
sigmau_0 = 1;
sigmabetau_0 =0.5;
gamma_0 = 0.2;

initial = [mubeta_0 muu_0 sigmabeta_0 sigmau_0 sigmabetau_0 gamma_0];

min1 = (initial(1)-3*initial(3)); 
max1 = (initial(1)+(3)*initial(3));
[node1, weight1] = qnwequi(100,min1,max1);

min2 = (initial(2)-3*initial(4)); 
max2 = (initial(2)+(3)*initial(4));
[node2, weight2] = qnwequi(100,min2,max2);

[parameters] = fminunc(@(parameters) (-1)*likelihood3(X,Y,Z,parameters,node1,node2,weight1,weight2),initial,options);
disp('The Likelihood function is maximized at parameters mu(beta), mu(u), sigma(beta), sigma(u), sigma(beta,u), gamma) ')
parameters


