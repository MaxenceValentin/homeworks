function L = likelihood3(X,Y,Z,parameters,node1,node2,weight1,weight2)
%This function assumes that u = sigma u = gamma = sigma u beta = 0

MU = [parameters(1), parameters(2)];
SIGMA = [parameters(3),parameters(5) ; parameters(5), parameters(4)]; 

F = @(x) 1/(1+exp(-x));

int1 = ones(100,1);
int2 = ones(100,1);
likelihood3=1;
i =1;
j=1;
t=1;
o=1;

for i = 1:100 %This loops over 100 N obserations

    for o = 1:length(weight2)%This loops over the nodes of u
        Lj = ones(length(weight1),1);
        phi = ones(length(weight1),1);
        for j = 1:length(weight1); %This loops over the nodes of beta
 
            for t= 1:20 %This loops over T observations
                Lt = (F(node1(j)*X(t,i)+parameters(6)*Z(t,i)+node2(o)))^(Y(t,i))*(1-F(node1(j)*X(t,i)+parameters(6)*Z(t,i)+node2(o)))^(1-Y(t,i));
                Lj(j) = Lj(j)*Lt;     
            end %I end up with 20 evaluation at nodes beta
        phi(j) = mvnpdf([node1(j),node2(o)],MU,SIGMA);
        end
        int1(o) = weight1'*(Lj.*phi);
   end
int2(i) = weight2'*int1;

L = likelihood3*int2(i);
end





