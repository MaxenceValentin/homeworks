function L = likelihood1(X,Y,parameter,node,weight)
%This function assumes that u = sigma u = gamma = sigma u beta = 0
% you cannot assume that since sigma_u must be estimated here
F = @(x) 1/(1+exp(-x));

int = ones(100,1);
likelihood1=1;
i =1;
j=1;
t=1;

for i = 1:100;
Lj = ones(length(weight),1);

for j = 1:length(weight); 
 
    for t= 1:20
        Lt = (F(node(j)*X(t,i)))^(Y(t,i))*(1-F(node(j)*X(t,i)))^(1-Y(t,i));
        Lj(j) = Lj(j)*Lt;     
    end
end
int(i) = weight'*(Lj.*normpdf(node,parameter(1),parameter(2)));
% This is not the right way to calculate integral. please take a look at
% lecture notes and answer key.
L = likelihood1*int(i)*1;
end