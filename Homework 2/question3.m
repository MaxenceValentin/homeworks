diary HM2_question3.out;

addpath('.\CEtools');

%Question 3 uses Gauss-Jacobi method to find the roots the the 3
%"independent" functions using different starting value. 


%Secant method is used within the Loop. I assumed that Broyden routine  in
%the 1 variable case is = to the secant method. 

tol = 1e-8;
optset('broyden','maxit',1000);
optset('broyden','tol',1e-8);
optset('broyden','showiters',0);

%%%%%Starting value 1,1,1%%%
tic
it =1;
a=1;
b=1;
c=1;
P=[a,b,c];
fa = @(x) exp(-1-x)/(1+exp(-1-x)+exp(-1-b)+exp(-1-c)) - x*exp(-1-x)/(1+exp(-1-x)+exp(-1-b)+exp(-1-c))*(1-exp(-1-x))/(1+exp(-1-x)+exp(-1-b)+exp(-1-c));
fb = @(y) exp(-1-y)/(1+exp(-1-a)+exp(-1-y)+exp(-1-c)) - y*exp(-1-y)/(1+exp(-1-a)+exp(-1-y)+exp(-1-c))*(1-exp(-1-y))/(1+exp(-1-a)+exp(-1-y)+exp(-1-c));
fc = @(z) exp(-1-z)/(1+exp(-1-a)+exp(-1-b)+exp(-1-z)) - z*exp(-1-z)/(1+exp(-1-a)+exp(-1-b)+exp(-1-z))*(1-exp(-1-z))/(1+exp(-1-a)+exp(-1-b)+exp(-1-z));

% again, these formulas do not seem correct. they produce the wrong result
% and I don't see any logic behind them.

disp('Starting value')
P

while (abs(fa(a))>tol && abs(fb(b))>tol && abs(fc(c))>tol);

x_sol = broyden(fa,a);
y_sol = broyden(fb,b);
z_sol = broyden(fc,c);
    
a=x_sol;
b=y_sol;
c=z_sol;
P=[a,b,c];

fa = @(x) exp(-1-x)/(1+exp(-1-x)+exp(-1-b)+exp(-1-c)) - x*exp(-1-x)/(1+exp(-1-x)+exp(-1-b)+exp(-1-c))*(1-exp(-1-x))/(1+exp(-1-x)+exp(-1-b)+exp(-1-c));
fb = @(y) exp(-1-y)/(1+exp(-1-a)+exp(-1-y)+exp(-1-c)) - y*exp(-1-y)/(1+exp(-1-a)+exp(-1-y)+exp(-1-c))*(1-exp(-1-y))/(1+exp(-1-a)+exp(-1-y)+exp(-1-c));
fc = @(z) exp(-1-z)/(1+exp(-1-a)+exp(-1-b)+exp(-1-z)) - z*exp(-1-z)/(1+exp(-1-a)+exp(-1-b)+exp(-1-z))*(1-exp(-1-z))/(1+exp(-1-a)+exp(-1-b)+exp(-1-z));

fa(a);
fb(b);
fc(c);

it = it+1;
if it>=1000
    break
end    
end
disp('Number of outter loop')
it
disp('Solutions P')
P
disp('Value of functions')
F=[fa(a),fb(b),fc(c)]
toc

%%%%%Starting value 0,0,0%%%
tic
it =1;
a=0;
b=0;
c=0;
P=[a,b,c];
fa = @(x) exp(-1-x)/(1+exp(-1-x)+exp(-1-b)+exp(-1-c)) - x*exp(-1-x)/(1+exp(-1-x)+exp(-1-b)+exp(-1-c))*(1-exp(-1-x))/(1+exp(-1-x)+exp(-1-b)+exp(-1-c));
fb = @(y) exp(-1-y)/(1+exp(-1-a)+exp(-1-y)+exp(-1-c)) - y*exp(-1-y)/(1+exp(-1-a)+exp(-1-y)+exp(-1-c))*(1-exp(-1-y))/(1+exp(-1-a)+exp(-1-y)+exp(-1-c));
fc = @(z) exp(-1-z)/(1+exp(-1-a)+exp(-1-b)+exp(-1-z)) - z*exp(-1-z)/(1+exp(-1-a)+exp(-1-b)+exp(-1-z))*(1-exp(-1-z))/(1+exp(-1-a)+exp(-1-b)+exp(-1-z));


disp('Starting value')
P
while (abs(fa(a))>tol && abs(fb(b))>tol && abs(fc(c))>tol);
x_sol = broyden(fa,a);
y_sol = broyden(fb,b);
z_sol = broyden(fc,c);
    
a=x_sol;
b=y_sol;
c=z_sol;
P=[a,b,c];

fa = @(x) exp(-1-x)/(1+exp(-1-x)+exp(-1-b)+exp(-1-c)) - x*exp(-1-x)/(1+exp(-1-x)+exp(-1-b)+exp(-1-c))*(1-exp(-1-x))/(1+exp(-1-x)+exp(-1-b)+exp(-1-c));
fb = @(y) exp(-1-y)/(1+exp(-1-a)+exp(-1-y)+exp(-1-c)) - y*exp(-1-y)/(1+exp(-1-a)+exp(-1-y)+exp(-1-c))*(1-exp(-1-y))/(1+exp(-1-a)+exp(-1-y)+exp(-1-c));
fc = @(z) exp(-1-z)/(1+exp(-1-a)+exp(-1-b)+exp(-1-z)) - z*exp(-1-z)/(1+exp(-1-a)+exp(-1-b)+exp(-1-z))*(1-exp(-1-z))/(1+exp(-1-a)+exp(-1-b)+exp(-1-z));

fa(a);
fb(b);
fc(c);


it = it+1;
if it>=1000
    break
end    
end
disp('Number of outter loop')
it
disp('Solutions P')
P
disp('Value of functions')
F=[fa(a),fb(b),fc(c)]
toc

%%%%%Starting value 0,1,2%%%
tic
it =1;
a=0;
b=1;
c=2;
P=[a,b,c];
fa = @(x) exp(-1-x)/(1+exp(-1-x)+exp(-1-b)+exp(-1-c)) - x*exp(-1-x)/(1+exp(-1-x)+exp(-1-b)+exp(-1-c))*(1-exp(-1-x))/(1+exp(-1-x)+exp(-1-b)+exp(-1-c));
fb = @(y) exp(-1-y)/(1+exp(-1-a)+exp(-1-y)+exp(-1-c)) - y*exp(-1-y)/(1+exp(-1-a)+exp(-1-y)+exp(-1-c))*(1-exp(-1-y))/(1+exp(-1-a)+exp(-1-y)+exp(-1-c));
fc = @(z) exp(-1-z)/(1+exp(-1-a)+exp(-1-b)+exp(-1-z)) - z*exp(-1-z)/(1+exp(-1-a)+exp(-1-b)+exp(-1-z))*(1-exp(-1-z))/(1+exp(-1-a)+exp(-1-b)+exp(-1-z));


disp('Starting value')
P
while (abs(fa(a))>tol && abs(fb(b))>tol && abs(fc(c))>tol);
x_sol = broyden(fa,a);
y_sol = broyden(fb,b);
z_sol = broyden(fc,c);
    
a=x_sol;
b=y_sol;
c=z_sol;
P=[a,b,c];

fa = @(x) exp(-1-x)/(1+exp(-1-x)+exp(-1-b)+exp(-1-c)) - x*exp(-1-x)/(1+exp(-1-x)+exp(-1-b)+exp(-1-c))*(1-exp(-1-x))/(1+exp(-1-x)+exp(-1-b)+exp(-1-c));
fb = @(y) exp(-1-y)/(1+exp(-1-a)+exp(-1-y)+exp(-1-c)) - y*exp(-1-y)/(1+exp(-1-a)+exp(-1-y)+exp(-1-c))*(1-exp(-1-y))/(1+exp(-1-a)+exp(-1-y)+exp(-1-c));
fc = @(z) exp(-1-z)/(1+exp(-1-a)+exp(-1-b)+exp(-1-z)) - z*exp(-1-z)/(1+exp(-1-a)+exp(-1-b)+exp(-1-z))*(1-exp(-1-z))/(1+exp(-1-a)+exp(-1-b)+exp(-1-z));

fa(a);
fb(b);
fc(c);


it = it+1;
if it>=1000
    break
end    
end
disp('Number of outter loop')
it
disp('Solutions P')
P
disp('Value of functions')
F=[fa(a),fb(b),fc(c)]
toc

%%%%%Starting value 3,2,1%%%
tic
it =1;
a=3;
b=2;
c=1;
P=[a,b,c];
fa = @(x) exp(-1-x)/(1+exp(-1-x)+exp(-1-b)+exp(-1-c)) - x*exp(-1-x)/(1+exp(-1-x)+exp(-1-b)+exp(-1-c))*(1-exp(-1-x))/(1+exp(-1-x)+exp(-1-b)+exp(-1-c));
fb = @(y) exp(-1-y)/(1+exp(-1-a)+exp(-1-y)+exp(-1-c)) - y*exp(-1-y)/(1+exp(-1-a)+exp(-1-y)+exp(-1-c))*(1-exp(-1-y))/(1+exp(-1-a)+exp(-1-y)+exp(-1-c));
fc = @(z) exp(-1-z)/(1+exp(-1-a)+exp(-1-b)+exp(-1-z)) - z*exp(-1-z)/(1+exp(-1-a)+exp(-1-b)+exp(-1-z))*(1-exp(-1-z))/(1+exp(-1-a)+exp(-1-b)+exp(-1-z));


disp('Starting value')
P
while (abs(fa(a))>tol && abs(fb(b))>tol && abs(fc(c))>tol);
x_sol = broyden(fa,a);
y_sol = broyden(fb,b);
z_sol = broyden(fc,c);
    
a=x_sol;
b=y_sol;
c=z_sol;
P=[a,b,c];

fa = @(x) exp(-1-x)/(1+exp(-1-x)+exp(-1-b)+exp(-1-c)) - x*exp(-1-x)/(1+exp(-1-x)+exp(-1-b)+exp(-1-c))*(1-exp(-1-x))/(1+exp(-1-x)+exp(-1-b)+exp(-1-c));
fb = @(y) exp(-1-y)/(1+exp(-1-a)+exp(-1-y)+exp(-1-c)) - y*exp(-1-y)/(1+exp(-1-a)+exp(-1-y)+exp(-1-c))*(1-exp(-1-y))/(1+exp(-1-a)+exp(-1-y)+exp(-1-c));
fc = @(z) exp(-1-z)/(1+exp(-1-a)+exp(-1-b)+exp(-1-z)) - z*exp(-1-z)/(1+exp(-1-a)+exp(-1-b)+exp(-1-z))*(1-exp(-1-z))/(1+exp(-1-a)+exp(-1-b)+exp(-1-z));

fa(a);
fb(b);
fc(c);

it = it+1;
if it>=1000
    break
end    
end
disp('Number of outter loop')
it
disp('Solutions P')
P
disp('Value of functions')
F=[fa(a),fb(b),fc(c)]
toc

%Iteration and time is very similar with respect to the starting value.
%Answers are also very different that from the Broyden method of question 2 
%as I get optimal prices at circa 1.40. I might have issue here. 
%Again, using values that diverge leads to issue of estimation (case 4).
diary off;