diary HM2_question2.out;

addpath('CEtools');
% Set options for Broyden
optset('broyden','showiters',1000);
optset('broyden','maxit',300) ;
optset('broyden','tol',1e-8) ;

%Sorry this is disgusting but couldnt make it work with vector input
f = @(P) [exp(-1-P(1))/(1+exp(-1-P(1))+exp(-1-P(2))+exp(-1-P(3))) - ...
        P(1)*exp(-1-P(1))/(1+exp(-1-P(1))+exp(-1-P(2))+exp(-1-P(3)))...
        *(1-exp(-1-P(1))/(1+exp(-1-P(1))+exp(-1-P(2))+exp(-1-P(3))));...
            exp(-1-P(2))/(1+exp(-1-P(1))+exp(-1-P(2))+exp(-1-P(3))) - ...
            P(2)*exp(-1-P(2))/(1+exp(-1-P(1))+exp(-1-P(2))+exp(-1-P(3)))...
            *(1-exp(-1-P(2))/(1+exp(-1-P(1))+exp(-1-P(2))+exp(-1-P(3))));...
                exp(-1-P(3))/(1+exp(-1-P(1))+exp(-1-P(2))+exp(-1-P(3))) - ...
                P(3)*exp(-1-P(3))/(1+exp(-1-P(1))+exp(-1-P(2))+exp(-1-P(3)))...
                *(1-exp(-1-P(3))/(1+exp(-1-P(1))+exp(-1-P(2))+exp(-1-P(3))))];
% I don't understand what this thing means. It does not work the right way
% also. take a look at the answer key

%%%%%Starting value 1,1,1%%%
tic
a=1;
b=1;
c=1;
P_start=[a,b,c];
disp('Starting value')
P_start
            
P = broyden(f,[1;1;1]);

disp('Solutions P')
P
disp('Value of functions')
f(P)
toc

%%%%%Starting value 0,0,0%%%
tic
a=0;
b=0;
c=0;
P_start=[a,b,c];
disp('Starting value')
P_start
           
P = broyden(f,[0;0;0]);

disp('Solutions P')
P
disp('Value of functions')
f(P)
toc

%%%%%Starting value 0,1,2%%%
tic
a=0;
b=1;
c=2;
P_start=[a,b,c];
disp('Starting value')
P_start
            
P = broyden(f,[0;1;2]);

disp('Solutions P')
P
disp('Value of functions')
f(P)
toc


%%%%%Starting value 3,2,1%%%
tic
a=3;
b=2;
c=1;
P_start=[a,b,c];
disp('Starting value')
P_start
  
P = broyden(f,[3;2;1]);

disp('Solutions P')
P
disp('Value of functions')
f(P)
toc

%The last two broyden routines return value of P which are inconsistent
%with the Nash equilibrium conditions. 
diary off;