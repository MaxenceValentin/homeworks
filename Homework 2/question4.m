%Question 4
diary HM2_question4.out;
tol = 1e-8;

%%%%%Starting value 1,1,1%%%
tic

a=1;
b=1;
c=1;
it=1;

P=[a,b,c];
disp('Starting value')
Pnew = [0,0,0];
P
while abs(P-Pnew)>tol 

Q=exp(-1-P)/(1+exp(-1-P(1))+exp(-1-P(2))+exp(-1-P(3)));

Pnew=1./(1.-Q);
P=Pnew;
it = it+1;
if it>=100
    break
end  
end
disp('Number of outter loop')
it
disp('Solutions P')
P
disp('Value of functions')
Q
toc

%%%%%Starting value 0,0,0%%%
tic

a=0;
b=0;
c=0;
it=1;
Pnew = [0,0,0];

P=[a,b,c];
disp('Starting value')
P
while abs(P-Pnew)>tol % try to evaluate this at the starting point. 
    % 1) this is vector-valued, second, it's all zeros. thus, your code deos not run even once
P=Pnew;
Q=exp(-1-P)/(1+exp(-1-P(1))+exp(-1-P(2))+exp(-1-P(3)));

Pnew=1./(1.-Q);


it = it+1;
if it>=100
    break
end  
end
disp('Number of outter loop')
it
disp('Solutions P')
Pnew
disp('Value of functions')
Q
toc

%%%%%Starting value 0,1,2%%%
tic

a=0;
b=1;
c=2;
it=1;
Pnew = [0,0,0];

P=[a,b,c]
disp('Starting value')
P
while abs(P-Pnew)>tol
P=Pnew;
Q=exp(-1-P)/(1+exp(-1-P(1))+exp(-1-P(2))+exp(-1-P(3)));

Pnew=1./(1.-Q);


it = it+1;
if it>=100
    break
end  
end
disp('Number of outter loop')
it
disp('Solutions P')
Pnew
disp('Value of functions')
Q
toc

%%%%%Starting value 3,2,1%%%
tic

a=3;
b=2;
c=1;
it=1;
Pnew = [0,0,0];

P=[a,b,c];
disp('Starting value')
P
while abs(P-Pnew)>tol
P=Pnew;
Q=exp(-1-P)/(1+exp(-1-P(1))+exp(-1-P(2))+exp(-1-P(3)));

Pnew=1./(1.-Q);

it = it+1;
if it>=10000
    break
end  
end
disp('Number of outter loop')
it
disp('Solutions P')
Pnew
disp('Value of functions')
Q
toc

%Regardless of the starting point, all routine converge to P = 1.0985 which
%is similar to the Broyden method results when starting with reasonable
%first values.
diary off;