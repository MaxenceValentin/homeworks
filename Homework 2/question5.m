diary HM2_question5.out;

addpath('CEtools');

%Guauss Jacobi method with only one secant step within the inner loop

tol = 1e-8;
optset('broyden','maxit',1);
optset('broyden','tol',1e-8);
optset('broyden','showiters',0);

%%%%%Starting value 1,1,1%%%
tic
it =1;
a=1;
b=1;
c=1;
P=[a,b,c];
fa = @(x) exp(-1-x)/(1+exp(-1-x)+exp(-1-b)+exp(-1-c)) - x*exp(-1-x)/(1+exp(-1-x)+exp(-1-b)+exp(-1-c))*(1-exp(-1-x))/(1+exp(-1-x)+exp(-1-b)+exp(-1-c));
fb = @(y) exp(-1-y)/(1+exp(-1-a)+exp(-1-y)+exp(-1-c)) - y*exp(-1-y)/(1+exp(-1-a)+exp(-1-y)+exp(-1-c))*(1-exp(-1-y))/(1+exp(-1-a)+exp(-1-y)+exp(-1-c));
fc = @(z) exp(-1-z)/(1+exp(-1-a)+exp(-1-b)+exp(-1-z)) - z*exp(-1-z)/(1+exp(-1-a)+exp(-1-b)+exp(-1-z))*(1-exp(-1-z))/(1+exp(-1-a)+exp(-1-b)+exp(-1-z));
% again, these formulas don't seem to work here again. take a look at the
% answer key.
disp('Starting value')
P

while (abs(fa(a))>tol && abs(fb(b))>tol && abs(fc(c))>tol);

x_sol = broyden(fa,a);
y_sol = broyden(fb,b);
z_sol = broyden(fc,c);
    
a=x_sol;
b=y_sol;
c=z_sol;
P=[a,b,c];

fa = @(x) exp(-1-x)/(1+exp(-1-x)+exp(-1-b)+exp(-1-c)) - x*exp(-1-x)/(1+exp(-1-x)+exp(-1-b)+exp(-1-c))*(1-exp(-1-x))/(1+exp(-1-x)+exp(-1-b)+exp(-1-c));
fb = @(y) exp(-1-y)/(1+exp(-1-a)+exp(-1-y)+exp(-1-c)) - y*exp(-1-y)/(1+exp(-1-a)+exp(-1-y)+exp(-1-c))*(1-exp(-1-y))/(1+exp(-1-a)+exp(-1-y)+exp(-1-c));
fc = @(z) exp(-1-z)/(1+exp(-1-a)+exp(-1-b)+exp(-1-z)) - z*exp(-1-z)/(1+exp(-1-a)+exp(-1-b)+exp(-1-z))*(1-exp(-1-z))/(1+exp(-1-a)+exp(-1-b)+exp(-1-z));

fa(a);
fb(b);
fc(c);

it = it+1;
if it>=1000
    break
end    
end
disp('Number of outter loop')
it
disp('Solutions P')
P
disp('Value of functions')
F=[fa(a),fb(b),fc(c)]
toc

%%%%%Starting value 0,0,0%%%
tic
it =1;
a=0;
b=0;
c=0;
P=[a,b,c];
fa = @(x) exp(-1-x)/(1+exp(-1-x)+exp(-1-b)+exp(-1-c)) - x*exp(-1-x)/(1+exp(-1-x)+exp(-1-b)+exp(-1-c))*(1-exp(-1-x))/(1+exp(-1-x)+exp(-1-b)+exp(-1-c));
fb = @(y) exp(-1-y)/(1+exp(-1-a)+exp(-1-y)+exp(-1-c)) - y*exp(-1-y)/(1+exp(-1-a)+exp(-1-y)+exp(-1-c))*(1-exp(-1-y))/(1+exp(-1-a)+exp(-1-y)+exp(-1-c));
fc = @(z) exp(-1-z)/(1+exp(-1-a)+exp(-1-b)+exp(-1-z)) - z*exp(-1-z)/(1+exp(-1-a)+exp(-1-b)+exp(-1-z))*(1-exp(-1-z))/(1+exp(-1-a)+exp(-1-b)+exp(-1-z));


disp('Starting value')
P
while (abs(fa(a))>tol && abs(fb(b))>tol && abs(fc(c))>tol);
x_sol = broyden(fa,a);
y_sol = broyden(fb,b);
z_sol = broyden(fc,c);
    
a=x_sol;
b=y_sol;
c=z_sol;
P=[a,b,c];

fa = @(x) exp(-1-x)/(1+exp(-1-x)+exp(-1-b)+exp(-1-c)) - x*exp(-1-x)/(1+exp(-1-x)+exp(-1-b)+exp(-1-c))*(1-exp(-1-x))/(1+exp(-1-x)+exp(-1-b)+exp(-1-c));
fb = @(y) exp(-1-y)/(1+exp(-1-a)+exp(-1-y)+exp(-1-c)) - y*exp(-1-y)/(1+exp(-1-a)+exp(-1-y)+exp(-1-c))*(1-exp(-1-y))/(1+exp(-1-a)+exp(-1-y)+exp(-1-c));
fc = @(z) exp(-1-z)/(1+exp(-1-a)+exp(-1-b)+exp(-1-z)) - z*exp(-1-z)/(1+exp(-1-a)+exp(-1-b)+exp(-1-z))*(1-exp(-1-z))/(1+exp(-1-a)+exp(-1-b)+exp(-1-z));

fa(a);
fb(b);
fc(c);


it = it+1;
if it>=1000
    break
end    
end
disp('Number of outter loop')
it
disp('Solutions P')
P
disp('Value of functions')
F=[fa(a),fb(b),fc(c)]
toc

%%%%%Starting value 0,1,2%%%
tic
it =1;
a=0;
b=1;
c=2;
P=[a,b,c];
fa = @(x) exp(-1-x)/(1+exp(-1-x)+exp(-1-b)+exp(-1-c)) - x*exp(-1-x)/(1+exp(-1-x)+exp(-1-b)+exp(-1-c))*(1-exp(-1-x))/(1+exp(-1-x)+exp(-1-b)+exp(-1-c));
fb = @(y) exp(-1-y)/(1+exp(-1-a)+exp(-1-y)+exp(-1-c)) - y*exp(-1-y)/(1+exp(-1-a)+exp(-1-y)+exp(-1-c))*(1-exp(-1-y))/(1+exp(-1-a)+exp(-1-y)+exp(-1-c));
fc = @(z) exp(-1-z)/(1+exp(-1-a)+exp(-1-b)+exp(-1-z)) - z*exp(-1-z)/(1+exp(-1-a)+exp(-1-b)+exp(-1-z))*(1-exp(-1-z))/(1+exp(-1-a)+exp(-1-b)+exp(-1-z));


disp('Starting value')
P
while (abs(fa(a))>tol && abs(fb(b))>tol && abs(fc(c))>tol);
x_sol = broyden(fa,a);
y_sol = broyden(fb,b);
z_sol = broyden(fc,c);
    
a=x_sol;
b=y_sol;
c=z_sol;
P=[a,b,c];

fa = @(x) exp(-1-x)/(1+exp(-1-x)+exp(-1-b)+exp(-1-c)) - x*exp(-1-x)/(1+exp(-1-x)+exp(-1-b)+exp(-1-c))*(1-exp(-1-x))/(1+exp(-1-x)+exp(-1-b)+exp(-1-c));
fb = @(y) exp(-1-y)/(1+exp(-1-a)+exp(-1-y)+exp(-1-c)) - y*exp(-1-y)/(1+exp(-1-a)+exp(-1-y)+exp(-1-c))*(1-exp(-1-y))/(1+exp(-1-a)+exp(-1-y)+exp(-1-c));
fc = @(z) exp(-1-z)/(1+exp(-1-a)+exp(-1-b)+exp(-1-z)) - z*exp(-1-z)/(1+exp(-1-a)+exp(-1-b)+exp(-1-z))*(1-exp(-1-z))/(1+exp(-1-a)+exp(-1-b)+exp(-1-z));

fa(a);
fb(b);
fc(c);


it = it+1;
if it>=1000
    break
end    
end
disp('Number of outter loop')
it
disp('Solutions P')
P
disp('Value of functions')
F=[fa(a),fb(b),fc(c)]
toc

%%%%%Starting value 3,2,1%%%
tic
it =1;
a=3;
b=2;
c=1;
P=[a,b,c];
fa = @(x) exp(-1-x)/(1+exp(-1-x)+exp(-1-b)+exp(-1-c)) - x*exp(-1-x)/(1+exp(-1-x)+exp(-1-b)+exp(-1-c))*(1-exp(-1-x))/(1+exp(-1-x)+exp(-1-b)+exp(-1-c));
fb = @(y) exp(-1-y)/(1+exp(-1-a)+exp(-1-y)+exp(-1-c)) - y*exp(-1-y)/(1+exp(-1-a)+exp(-1-y)+exp(-1-c))*(1-exp(-1-y))/(1+exp(-1-a)+exp(-1-y)+exp(-1-c));
fc = @(z) exp(-1-z)/(1+exp(-1-a)+exp(-1-b)+exp(-1-z)) - z*exp(-1-z)/(1+exp(-1-a)+exp(-1-b)+exp(-1-z))*(1-exp(-1-z))/(1+exp(-1-a)+exp(-1-b)+exp(-1-z));


disp('Starting value')
P
while (abs(fa(a))>tol && abs(fb(b))>tol && abs(fc(c))>tol);
x_sol = broyden(fa,a);
y_sol = broyden(fb,b);
z_sol = broyden(fc,c);
    
a=x_sol;
b=y_sol;
c=z_sol;
P=[a,b,c];

fa = @(x) exp(-1-x)/(1+exp(-1-x)+exp(-1-b)+exp(-1-c)) - x*exp(-1-x)/(1+exp(-1-x)+exp(-1-b)+exp(-1-c))*(1-exp(-1-x))/(1+exp(-1-x)+exp(-1-b)+exp(-1-c));
fb = @(y) exp(-1-y)/(1+exp(-1-a)+exp(-1-y)+exp(-1-c)) - y*exp(-1-y)/(1+exp(-1-a)+exp(-1-y)+exp(-1-c))*(1-exp(-1-y))/(1+exp(-1-a)+exp(-1-y)+exp(-1-c));
fc = @(z) exp(-1-z)/(1+exp(-1-a)+exp(-1-b)+exp(-1-z)) - z*exp(-1-z)/(1+exp(-1-a)+exp(-1-b)+exp(-1-z))*(1-exp(-1-z))/(1+exp(-1-a)+exp(-1-b)+exp(-1-z));

fa(a);
fb(b);
fc(c);

it = it+1;
if it>=1000
    break
end    
end
disp('Number of outter loop')
it
disp('Solutions P')
P
disp('Value of functions')
F=[fa(a),fb(b),fc(c)]
toc

%Results are unchanged from using only one iteration versus unlimited
%itteration in the secant method (inner loop). However, it appears to take
%longer than the unrestricted version. It also appears that more outer
%loops are needed to find the roots.


diary off;