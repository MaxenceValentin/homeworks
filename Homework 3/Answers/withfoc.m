function [f,df] = withfoc(B,X,y,R)

f = -(sum(-exp(X*B')+y.*X*B'-log(factorial(y))));

%foc
 df = [sum(X(:,1).*exp(X*B'))-sum(R(1));sum(X(:,2).*exp(X*B'))-sum(R(2));sum(X(:,3).*exp(X*B'))-sum(R(3));sum(X(:,4).*exp(X*B'))-sum(R(4));sum(X(:,5).*exp(X*B'))-sum(R(5));sum(X(:,6).*exp(X*B'))-sum(R(6))];
end