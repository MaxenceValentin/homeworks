addpath('C:\Users\mav24\Desktop\Working documents\Econ 512\Homework 3\CEtools');
% please don't use the absolute path, its different on my computer. use the
% relative path

%%Generate Gamma distribution
rng(3145);
A= rand(1,1)*10;
B = rand(1,1)*2;
R = gamrnd(A,B,1000,1); 


%%%Get statistics needed for MLE
n = numel(R);
Y1= sum(R)/n;
Y2 = exp(1/n*sum(log(R)));

%functions
f = @(a) exp(psi(a))/a - Y2/Y1; %function for which we need the root

%%%compute theta1, theta2 and var/covar matrix
a = fsolve(f,1);
% you were not supposed to use any generic root finder, you needed to code
% your own
b = a/Y1;
var  = inv([(-1*trigamma(a)) n/b ; n/b -n*a/b^2]);


disp('Theta1');
a
disp('Theta2');
b
disp('Var/Covar Matrix');
var

%%%%%%PLOT Theta 1 against Y1/Y2
x = linspace(1.1,3,1000);
for i = 1:1000
    f = @(t) t/exp(psi(t)) - x(i);
    y(i) = fsolve(f,1);
end
plot(x,y)