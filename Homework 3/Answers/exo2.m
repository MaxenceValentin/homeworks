addpath('C:\Users\mav24\Desktop\Working documents\Econ 512\Homework 3\CEtools');

%initial value
B = [0 0 0 0 0 0];
R = y.*X;

%ML function to maximize
MLfun = @(B) sum(-exp(X*B')+y.*X*B'-log(factorial(y)));
%ML function to miminize (for Matlab routine)
MLfunmin = @(B) -sum(-exp(X*B')+y.*X*B'-log(factorial(y)));
%Non-linear LS function to minimize
NLLSfun = @(B) sum((y-exp(X*B')).^2);

%%
%%%%%%MLE using fminunc
options = optimoptions('fminunc','Display','iter','GradObj','off','Algorithm','quasi-newton','HessUpdate','bfgs');
tic
B1 = fminunc(MLfunmin,B,options);
toc

%%
%%%%%%Using fminunc providing derivative ! I cant make it work
options = optimoptions('fminunc','Display','iter','GradObj','on','Algorithm','quasi-newton','HessUpdate','bfgs');
tic
%B2 = fminunc('withfoc',[B X y R],options); 
toc

%%
%Nelder Mead method
tic
B3 = fminsearch(MLfunmin,B);
toc

%%
%BHHH Maximum likelihood


%%
%Using NLLS function to minimize it
[B5,fval,exitflag,output,grad,hessian2] = fminunc(NLLSfun,B);

%%
%Results
disp('Betas unsing MFMINUNC');
B1
disp('Betas unsing MFMINUNC providing derivatives');
%B2
disp('Betas unsing NelderMead');
B3
disp('Betas unsing BHHH routine');
%B4
disp('Betas Standard errors of the BHHH routine');
%sqrt(diag(inv(hessian1)))'
disp('Betas unsing NLLS method');
B5
disp('Betas Standard errors of the NLLS routine');
sqrt(diag(inv(hessian2)))'
